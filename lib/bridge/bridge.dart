// Abstraction: Shape
abstract class Shape {
  final DrawingBridge drawingBridge;

  Shape(this.drawingBridge);

  void draw();
}

// Implementation: Drawing Bridge
abstract class DrawingBridge {
  void drawCircle(double x, double y, double radius);
  // Other drawing methods...
}

// Concrete implementation 1: Drawing on screen
class DrawingOnScreen implements DrawingBridge {

  @override
  void drawCircle(double x, double y, double radius) {
    print('Drawing circle on screen at ($x, $y) with radius $radius');
    // Other drawing methods for screen...
  }

}

// Concrete implementation 2: Drawing on file

class DrawingOnFile implements DrawingBridge {
  @override
  void drawCircle(double x, double y, double radius) {
    print('Drawing circle in file at ($x, $y) with radius $radius');
  }
  // Other drawing methods for file...
  
}

// Concrete shape: Circle
class Circle extends Shape {
  final double x, y, radius;

  Circle(super.drawingBridge, {
   required this.x,
   required this.y,
   required this.radius
});

  @override
  void draw() {
    drawingBridge.drawCircle(x, y, radius);
  }

}