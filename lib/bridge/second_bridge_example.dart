// Interface for the implementation
abstract class Device {
  bool isEnabled();
  void enable();
  void disable();
  int getVolume();
  void setVolume(int percent);
  int getChannel();
  void setChannel(int channel);
}

// Abstraction class maintaining a reference to the implementation
class RemoteControl {
  final Device device;

  RemoteControl({
    required this.device,
  });

  void togglePower() {
    if (device.isEnabled()) {
      device.disable();
    } else {
      device.enable();
    }
  }

  void volumeDown() {
    device.setVolume(device.getVolume() - 10);
  }

  void volumeUp() {
    device.setVolume(device.getVolume() + 10);
  }

  void channelDown() {
    device.setChannel(device.getChannel() - 1);
  }

  void channelUp() {
    device.setChannel(device.getChannel() + 1);
  }
}

// Extended remote control with additional functionality
class AdvancedRemoteControl extends RemoteControl {
  AdvancedRemoteControl({required super.device});

  void mute() {
    device.setVolume(0);
  }
}

// TV implementation of Device interface
class TV implements Device {

  @override
  bool isEnabled() {
    // Implement isEnabled for TV
    return true;
  }

  @override
  void enable() {
    // Implement enable for TV
  }

  @override
  void disable() {
    // Implement disable for TV
  }

  @override
  int getVolume() {
    // Implement getVolume for TV
    return 50;
  }

  @override
  void setVolume(int percent) {
    // Implement setVolume for TV
  }

  @override
  int getChannel() {
    // Implement getChannel for TV
    return 5;
  }

  @override
  void setChannel(int channel) {
    // Implement setChannel for TV
  }
}

// Radio implementation of Device interface
class RadioDevice implements Device {
  @override
  bool isEnabled() {
    // Implement isEnabled for Radio
    return false;
  }

  @override
  void enable() {
    // Implement enable for Radio
  }

  @override
  void disable() {
    // Implement disable for Radio
  }

  @override
  int getVolume() {
    // Implement getVolume for Radio
    return 30;
  }

  @override
  void setVolume(int percent) {
    // Implement setVolume for Radio
  }

  @override
  int getChannel() {
    // Implement getChannel for Radio
    return 10;
  }

  @override
  void setChannel(int channel) {
    // Implement setChannel for Radio
  }
}