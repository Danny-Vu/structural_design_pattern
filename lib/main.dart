import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:structural_design_pattern/adapter/adapter.dart';
import 'package:structural_design_pattern/bridge/bridge.dart';
import 'package:structural_design_pattern/bridge/second_bridge_example.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  StatelessElement createElement() {
    //_adapter();
    //_bridge();
    _bridgeSecondExample();
    return super.createElement();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green,
    );
  }

  /// Adapter
  void _adapter() {
// Using the old player
    OldMusicPlayer oldPlayer = OldPlayer();
    oldPlayer.playMusic(); // Output: Old player is playing music.

    // Using the new player directly (without adapter)
    NewMusicPlayer newPlayer = NewPlayer();
    newPlayer.play(); // Output: New player plays music.

    // Using the adapter to make the new player compatible with the old system
    OldMusicPlayer adaptedPlayer = Adapter(newMusicPlayer: NewPlayer());
    adaptedPlayer
        .playMusic(); // Output: New player plays music. (Through adapter)
  }

  /// Bridge
  void _bridge() {
    // Drawing on screen
    final DrawingBridge screenBridge = DrawingOnScreen();
    final Shape circleOnScreen = Circle(x:10, y: 20, radius: 30, screenBridge,);
    circleOnScreen.draw(); // Output: Drawing circle on screen at (10, 20) with radius 30

    // Drawing in file
    final DrawingBridge fileBridge = DrawingOnFile();
    final Shape circleInFile = Circle(x:100, y: 200, radius:50, fileBridge);
    circleInFile.draw(); // Output: Drawing circle in file at (100, 200) with radius 50
  }

  /// The second example of Bridge
  void _bridgeSecondExample() {
    var tv = TV();
    var remoteControl = RemoteControl(device: tv);
    remoteControl.togglePower();

    var radio = RadioDevice();
    var advancedRemote = AdvancedRemoteControl(device: radio);
    advancedRemote.mute();
  }
}
