/*
Adapter is a structural design pattern that allows objects
with incompatible interfaces to collaborate.
 */
// Old music player interface
abstract class OldMusicPlayer {
  void playMusic();
}

// Old music player implementation
class OldPlayer extends OldMusicPlayer {

  @override
  void playMusic() {
    print("Old player can play old music instrument");
  }

}

// New music player interface
abstract class NewMusicPlayer {
  void play();
}

// New music player implementation
class NewPlayer extends NewMusicPlayer {
  @override
  void play() {
    print("New player can play new music instrument");
  }

}

// Adapter to make NewPlayer compatible with OldMusicPlayer
class Adapter implements OldMusicPlayer {

  final NewMusicPlayer newMusicPlayer;

  Adapter({required this.newMusicPlayer});

  @override
  void playMusic() {
    newMusicPlayer.play();
  }

}
